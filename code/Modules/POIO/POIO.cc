//------------------------------------------------------------------------------
//  POIO.cc
//------------------------------------------------------------------------------
#include "Pre.h"
#include "POIO.h"
#include "Core/Core.h"

namespace Oryol {

using namespace _priv;

POIO::_state* POIO::state = nullptr;

//------------------------------------------------------------------------------
void
POIO::Setup() {
    o_assert(!IsValid());
    state = Memory::New<_state>();
    state->router.setup();
    state->runLoopId = Core::PreRunLoop()->Add([] { doWork(); });
}

//------------------------------------------------------------------------------
void
POIO::Discard() {
    o_assert(IsValid());
    Core::PreRunLoop()->Remove(state->runLoopId);
    state->router.discard();
    Memory::Delete(state);
    state = nullptr;
}

//------------------------------------------------------------------------------
void
POIO::doWork() {
    o_assert_dbg(IsValid());
    o_assert_dbg(Core::IsMainThread());
    state->router.doWork();
    state->loadQueue.update();
}

//------------------------------------------------------------------------------
bool
POIO::IsValid() {
    return nullptr != state;
}

void
POIO::SendQuery(const HttpQuery& query, LoadSuccessFunc onSuccess, LoadFailedFunc onFailed) {
    o_assert_dbg(IsValid());
    Log::Info("enter POIO::SendQuery \n");
    state->loadQueue.add(query, onSuccess, onFailed);
}

//------------------------------------------------------------------------------
int
POIO::NumPendingLoads() {
    o_assert_dbg(IsValid());
    return state->loadQueue.numPending();
}

//------------------------------------------------------------------------------
//Ptr<IORead>
//IO::LoadFile(const URL& url) {
//    o_assert_dbg(IsValid());
//    Ptr<IORead> ioReq = IORead::Create();
//    ioReq->Url = url;
//    state->router.put(ioReq);
//    return ioReq;
//}

//------------------------------------------------------------------------------
//Ptr<IOWrite>
//IO::WriteFile(const URL& url, const Buffer& data) {
//    o_assert_dbg(IsValid());
//    Ptr<IOWrite> ioReq = IOWrite::Create();
//    ioReq->Url = url;
//    ioReq->Data.Add(data.Data(), data.Size());
//    state->router.put(ioReq);
//    return ioReq;
//}

//------------------------------------------------------------------------------
void
POIO::Put(const Ptr<POIORequest>& ioReq) {

  Log::Info("enter POIO::Put(const Ptr<POIORequest>& ioReq) \n");
  //maintenant je veux voir si la requete est bien piquee pour etre transmise
    o_assert_dbg(IsValid());
    state->router.put(ioReq);
}

} // namespace Oryol
