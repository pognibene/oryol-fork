#pragma once
//------------------------------------------------------------------------------
/**
    @defgroup IO POIO
    @brief loading data asynchronously through pluggable filesystems

    @class Oryol::POIO
    @ingroup IO
    @brief IO module facade
*/

#include "Core/String/String.h"
#include "Core/String/StringAtom.h"


#include "POIO/FS/POioRouter.h"
#include "POIO/Core/POloadQueue.h"
#include "POIO/Core/HttpQuery.h"

#include "Core/RunLoop.h"

namespace Oryol {

class POIO {
public:
    /// setup the IO module
    static void Setup();

    /// discard the IO module
    static void Discard();

    /// check if IO module is valid
    static bool IsValid();
    
    /// success-callback for Load()
    typedef POloadQueue::successFunc LoadSuccessFunc;

    /// failed-callback for Load functions
    typedef POloadQueue::failFunc LoadFailedFunc;
    /// result of an asynchronous loading operation
    typedef POloadQueue::result LoadResult;
    
    static void SendQuery(const HttpQuery& query, LoadSuccessFunc onSuccess,
                          LoadFailedFunc onFailed=LoadFailedFunc());

    static int NumPendingLoads();

    // low-level: push a generic asynchronous IO request
    static void Put(const Ptr<POIORequest>& ioReq);
    
private:
    /// pump the ioRequestRouter
    static void doWork();

    struct _state {
        _priv::POioRouter router;
        RunLoop::Id runLoopId = RunLoop::InvalidId;
        class POloadQueue loadQueue;
    };
    static _state* state;
};

} // namespace Oryol
