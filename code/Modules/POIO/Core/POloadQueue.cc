//------------------------------------------------------------------------------
//  POloadQueue.cc
//------------------------------------------------------------------------------
#include "Pre.h"
#include "Core/Core.h"
#include "Core/RunLoop.h"

#include "POIO/POIO.h"
#include "POIO/Core/POloadQueue.h"

namespace Oryol {

//------------------------------------------------------------------------------
void
POloadQueue::add(const HttpQuery & query, successFunc onSuccess, failFunc onFail) {

  Log::Info("enter POloadQueue::add \n");
    o_assert_dbg(onSuccess);

    Ptr<POIORequest> ioReq = POIORequest::Create();

    //TODO etendre ioreq pour stocker les infos necessaires
    //TODO copier les infos de http request ici
    //car la requete est vide
    
    ioReq->url = query.url;
    ioReq->port = query.port;
    ioReq->type = query.queryType;
    ioReq->headers = query.headers;
    ioReq->urlParams = query.urlParams;

    //TODO see how to copy Data buffer (or get a reference on it)
    //think I have an example in the Write operation
    
    POIO::Put(ioReq);
    this->items.Add(item{ ioReq, onSuccess, onFail });
}


//------------------------------------------------------------------------------
int
POloadQueue::numPending() const {
    return this->items.Size();
}

//------------------------------------------------------------------------------
void
POloadQueue::update() {

    // check single items
    for (int i = this->items.Size() - 1; i >= 0; --i) {
        const item& curItem = this->items[i];
        const auto& ioReq = curItem.ioRequest;
        if (ioReq->Handled) {
            // io request has been handled
            if (POIOStatus::OK == ioReq->Status) {
                // io request was successful
                curItem.onSuccess(result(ioReq->url, std::move(ioReq->Data)));
            }
            else {
                // io request failed
                if (curItem.onFail) {
                    curItem.onFail(ioReq->url, ioReq->Status);
                }
                else {
                    // no fail handler was set, just print a warning
                    o_warn("loadQueue:: failed to load file '%s' with '%s'\n",
                        ioReq->url.AsCStr(), POIOStatus::ToString(ioReq->Status));
//FIXME plus de parametres que ça
//surtout si upload de fichier
                }
            }
            // remove the handled io request from the queue
            this->items.Erase(i);
        }
    }
}

} // namespace Oryol

