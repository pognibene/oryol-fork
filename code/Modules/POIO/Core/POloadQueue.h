#pragma once
//------------------------------------------------------------------------------
/**
    @class Oryol::POloadQueue
    @ingroup IO
    @brief asynchronously load multiple files, invoke callbacks with result
NOTE : PUT, GET, POST http operations should be supported

    This is the class behind the POIO::Load() and LoadGroup() functions.
*/
#include "Core/Types.h"
#include "Core/String/StringAtom.h"
#include "Core/Containers/Array.h"
#include "Core/Containers/Buffer.h"


#include "POIO/Core/POIOStatus.h"
#include "POIO/FS/POioRequests.h"
#include "POIO/Core/HttpQuery.h"

#include <functional>

namespace Oryol {
    
class POloadQueue {
public:
    /// loading result (if successful)
    struct result {
        result(const String & url, Buffer&& data) : Url(url), Data(std::move(data)) { };
        result(result&& rhs) {
            this->Url = std::move(rhs.Url);
            this->Data = std::move(rhs.Data);
        };
        void operator=(result&& rhs) {
            this->Url = std::move(rhs.Url);
            this->Data = std::move(rhs.Data);            
        };
        String Url;
        Buffer Data;
    };

    /// callback function signature for success
    typedef std::function<void(result result)> successFunc;
    
    /// callback function signature for failure
    typedef std::function<void(const String & url, POIOStatus::Code ioStatus)> failFunc;

    /// add an HTTP request to the queue
    void add(const HttpQuery & query, successFunc onSuccess, failFunc onFail=failFunc());
        
    /// update the queue, called per frame from runloop
    void update();

    /// get number of pending load actions
    int numPending() const;

private:
    struct item {
        Ptr<POIORequest> ioRequest;
        successFunc onSuccess;
        failFunc onFail;
    };
    Array<item> items;
};

} // namespace Oryol
