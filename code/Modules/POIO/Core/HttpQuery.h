#pragma once

#include "Core/String/String.h"
#include "Core/Containers/Array.h"
#include "Core/Containers/Buffer.h"

namespace Oryol {

/**
 * @brief The HttpQuery class
 */
class HttpQuery {
public:
  HttpQuery();
  
  void addUrlParameter(String & param);
  void addHeader(const char *header);
  void attachFile(Buffer & file, String & fileType);
  void setBody(String & body);

  String encodeUrl () const;

  String queryType;
  String url;
  int port = 0;
  Array<String> headers;

  // list of url parameters to encode
    Array<String> urlParams;
    //TODO : should be able to add a base64 encoded header
    //for basic auth
private:
    //TODO the url is to be composed with elements
    //like the base url, the url parameters, and so on
    //(parameters to be encoded)
    
    

    // filled with the binary data of the attachment, if there's one
    Buffer attachment;

    // list of headers to include in the request
    

    

    //TODO form params also


};
}
