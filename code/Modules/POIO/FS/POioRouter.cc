//------------------------------------------------------------------------------
//  POioRouter.cc
//------------------------------------------------------------------------------
#include "Pre.h"
#include "POioRouter.h"

namespace Oryol {
namespace _priv {

//------------------------------------------------------------------------------
void
POioRouter::setup() {
    for (auto& worker : this->workers) {
        worker.start();
    }
}

//------------------------------------------------------------------------------
void
POioRouter::discard() {
    for (auto& worker : this->workers) {
        worker.stop();
    }
}

//------------------------------------------------------------------------------
void
POioRouter::doWork() {
    for (auto& worker : this->workers) {
        worker.doWork();
    }
}

//------------------------------------------------------------------------------
//FIXME pas sur qu'on ait besoin du type POioMsg
//si j'utilise le meme type partout
void
POioRouter::put(const Ptr<POioMsg>& msg) {


  
        // for all network messages, use a round-robin dispatch
        this->curWorker = (this->curWorker + 1) % POIOConfig::NumWorkers;

	Log::Info("POioRouter::put in worker %d \n", this->curWorker);
	
        this->workers[this->curWorker].put(msg);    
}

} // namespace _priv
} // namespace Oryol


