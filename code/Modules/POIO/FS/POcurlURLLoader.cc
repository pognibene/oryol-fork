//------------------------------------------------------------------------------
//  POcurlURLLoader.cc
//------------------------------------------------------------------------------
#include "Pre.h"
#include "POcurlURLLoader.h"
#include "Core/String/StringConverter.h"
#include "Core/Containers/Buffer.h"
#include "curl/curl.h"

//#if LIBCURL_VERSION_NUM != 0x072400
//#error "Not using the right curl version, header search path fuckup?"
//#endif

namespace Oryol {
namespace _priv {

bool POcurlURLLoader::curlInitCalled = false;
std::mutex POcurlURLLoader::curlInitMutex;

//------------------------------------------------------------------------------
POcurlURLLoader::POcurlURLLoader() :
curlSession(0),
curlError(0) {

    // we need to do some one-time curl initialization here,
    // thread-protected because curl_global_init() is not thread-safe
    curlInitMutex.lock();
    if (!curlInitCalled) {
        CURLcode curlInitRes = curl_global_init(CURL_GLOBAL_ALL);
        o_assert(0 == curlInitRes);
        curlInitCalled = true;
    }
    curlInitMutex.unlock();

    // setup a new curl session
    this->setupCurlSession();
}

//------------------------------------------------------------------------------
POcurlURLLoader::~POcurlURLLoader() {
    this->discardCurlSession();
}

//------------------------------------------------------------------------------
void
POcurlURLLoader::setupCurlSession() {


    //FIXME a enlever, test
    //appelé 4 fois, donc on créé bien 4 session curls et 4 loaders
    //chaque loader est monothread
    //je dois faire quelque chose d'équivalent pour mon http
    Log::Warn("POcurlURLLoader::setupCurlSession APPEL\n");


    o_assert(0 == this->curlError);
    o_assert(0 == this->curlSession);

    // setup the error buffer
    const int curlErrorBufferSize = CURL_ERROR_SIZE * 4;
    this->curlError = (char*) Memory::Alloc(curlErrorBufferSize);
    Memory::Clear(this->curlError, curlErrorBufferSize);

    // setup the curl session
    this->curlSession = curl_easy_init();
    o_assert(0 != this->curlSession);

    // set session options
    curl_easy_setopt(this->curlSession, CURLOPT_NOSIGNAL, 1L);
    curl_easy_setopt(this->curlSession, CURLOPT_NOPROGRESS, 1L);
    curl_easy_setopt(this->curlSession, CURLOPT_ERRORBUFFER, this->curlError);
    curl_easy_setopt(this->curlSession, CURLOPT_WRITEFUNCTION, curlWriteDataCallback);
    curl_easy_setopt(this->curlSession, CURLOPT_TCP_KEEPALIVE, 1L);
    curl_easy_setopt(this->curlSession, CURLOPT_TCP_KEEPIDLE, 10L);
    curl_easy_setopt(this->curlSession, CURLOPT_TCP_KEEPINTVL, 10L);
    curl_easy_setopt(this->curlSession, CURLOPT_TIMEOUT, 30);
    curl_easy_setopt(this->curlSession, CURLOPT_CONNECTTIMEOUT, 30);
    curl_easy_setopt(this->curlSession, CURLOPT_ACCEPT_ENCODING, "");   // all encodings supported by curl
    curl_easy_setopt(this->curlSession, CURLOPT_FOLLOWLOCATION, 1);
}

//------------------------------------------------------------------------------
void
POcurlURLLoader::discardCurlSession() {
    o_assert(0 != this->curlError);
    o_assert(0 != this->curlSession);

    curl_easy_cleanup(this->curlSession);
    this->curlSession = 0;
    Memory::Free(this->curlError);
    this->curlError = 0;
}

//------------------------------------------------------------------------------
size_t
POcurlURLLoader::curlWriteDataCallback(char* ptr, size_t size, size_t nmemb, void* userData) {
    // userData is expected to point to a Buffer object
    int bytesToWrite = (int) (size * nmemb);
    if (bytesToWrite > 0) {
        Buffer* buf = (Buffer*) userData;
        buf->Add((const uint8_t*)ptr, bytesToWrite);
        return bytesToWrite;
    }
    else {
        return 0;
    }
}

//------------------------------------------------------------------------------
bool
POcurlURLLoader::doRequest(const Ptr<POIORequest>& req) {
Log::Info("enter POcurlURLLoader::doRequest \n");
 
    if (req->Cancelled) {
        req->Status = POIOStatus::Cancelled;
        req->Handled = true;
        return false;
    } else {
        this->doRequestInternal(req);
        req->Handled = true;
        return true;
    }
}

//------------------------------------------------------------------------------
void
POcurlURLLoader::doRequestInternal(const Ptr<POIORequest>& req) {

  Log::Info("enter POcurlURLLoader::doRequestInternal \n");

  
    o_assert(0 != this->curlSession);
    o_assert(0 != this->curlError);

    // set URL in curl
    //const URL& url = req->Url;
    //o_assert(url.Scheme() == "http");
    curl_easy_setopt(this->curlSession, CURLOPT_URL,
		     req->url.AsCStr());
    if (req->port) {
      //uint16_t port = StringConverter::FromString<uint16_t>(url.Port());
      uint16_t port = req->port;
        curl_easy_setopt(this->curlSession, CURLOPT_PORT, port);
    }

    //TODO switch on protocol
    curl_easy_setopt(this->curlSession, CURLOPT_HTTPGET, 1);

    // add standard request headers:
    //  User-Agent: need a 'standard' user-agent, otherwise some HTTP servers
    //              won't accept Connection: keep-alive
    //  Connection: keep-alive, don't open/close the connection all the time
    //  Accept-Encoding:    gzip, deflate
    //
    struct curl_slist* requestHeaders = 0;
    //requestHeaders = curl_slist_append(requestHeaders, "User-Agent: Mozilla/5.0");
    //requestHeaders = curl_slist_append(requestHeaders, "Connection: keep-alive");
    //requestHeaders = curl_slist_append(requestHeaders, "Accept-Encoding: gzip, deflate");

    Log::Info("NOMBRE DE HEADERS %d \n", req->headers.Size());

    // add custom headers
    for(const auto & s : req->headers) {
      Log::Info("AJOUT HEADER %s \n", s.AsCStr());
      requestHeaders = curl_slist_append(requestHeaders, s.AsCStr());
    }

    
    curl_easy_setopt(this->curlSession, CURLOPT_HTTPHEADER, requestHeaders);

    // prepare the HTTPResponse and the response-body stream
    curl_easy_setopt(this->curlSession, CURLOPT_WRITEDATA, &(req->Data));

    // perform the request
    CURLcode performResult = curl_easy_perform(this->curlSession);

    // query the http code
    long curlHttpCode = 0;
    curl_easy_getinfo(this->curlSession, CURLINFO_RESPONSE_CODE, &curlHttpCode);
    req->Status = (POIOStatus::Code) curlHttpCode;

    // check for error codes
    if (CURLE_PARTIAL_FILE == performResult) {
        // this seems to happen quite often even though all data has been received,
        // not sure what to do about this, but don't treat it as an error
      Log::Warn("curlURLLoader: CURLE_PARTIAL_FILE received for '%s', httpStatus='%ld'\n", req->url.AsCStr(), curlHttpCode);
        req->ErrorDesc = this->curlError;
    }
    else if (0 != performResult) {
        // some other curl error
        Log::Warn("curlURLLoader: curl_easy_peform failed with '%s' for '%s', httpStatus='%ld'\n",
		  req->url.AsCStr(), this->curlError, curlHttpCode);
        req->ErrorDesc = this->curlError;
    }

    // free the previously allocated request headers
    if (0 != requestHeaders) {
        curl_slist_free_all(requestHeaders);
        requestHeaders = 0;
    }
}

} // namespace _priv
} // namespace Oryol
