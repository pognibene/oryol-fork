#pragma once

#include "POioRequests.h"
#include "POurlLoader.h"

namespace Oryol {

  class POHTTPSystem {
  public:
    POHTTPSystem();
    ~POHTTPSystem();
    void onMsg(const Ptr<POIORequest>& ioReq);
  private:
    _priv::POurlLoader loader;
  };
}
