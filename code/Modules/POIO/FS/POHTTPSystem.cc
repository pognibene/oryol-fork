#include "POHTTPSystem.h"

namespace Oryol {

  POHTTPSystem::POHTTPSystem() {
    // empty
  }

  POHTTPSystem::~POHTTPSystem() {
    // empty
  }

  void POHTTPSystem::onMsg(const Ptr<POIORequest>& ioReq) {
   
    Log::Info("enter  POHTTPSystem::onMsg \n");

    this->loader.doRequest(ioReq);
  }

}
