#pragma once

#include "Core/Ptr.h"
#include "POIO/FS/POioRequests.h"
#include <mutex>

namespace Oryol {
namespace _priv {

class POcurlURLLoader {
public:
    /// constructor
    POcurlURLLoader();
    /// destructor
    ~POcurlURLLoader();
    /// process one request
    //bool doRequest(const Ptr<POIORead>& req);
    bool doRequest(const Ptr<POIORequest>& req);

private:
    /// setup curl session
    void setupCurlSession();
    /// discard the curl session
    void discardCurlSession();
    /// process one request (internal)
    //void doRequestInternal(const Ptr<POIORead>& req);
    void doRequestInternal(const Ptr<POIORequest>& req);
    /// curl write-data callback
    static size_t curlWriteDataCallback(char* ptr, size_t size, size_t nmemb, void* userData);
    /// curl header-data callback
    static size_t curlHeaderCallback(char* ptr, size_t size, size_t nmenb, void* userData);

    static bool curlInitCalled;
    static std::mutex curlInitMutex;
    void* curlSession;
    char* curlError;
};

} // namespace _priv
} // namespace Oryol


