#pragma once
//------------------------------------------------------------------------------
/**
    @file POIO/FS/POIORequests.h
    @brief IO request classes header
    
    These are all the core IO requests classes. If you want to 
    add custom requests to your own filesystem implementations, please
    add those custom requests in your own module, not here.
*/
#include "Core/RefCounted.h"
#include "Core/Containers/Buffer.h"
#include "Core/Containers/Array.h"

#include "Core/String/String.h"
#include "POIO/Core/POIOStatus.h"

namespace Oryol {
namespace _priv {
//------------------------------------------------------------------------------
class POioMsg : public RefCounted {
    OryolClassDecl(POioMsg);
    OryolBaseTypeDecl(POioMsg);
public:
    POioMsg() : Handled(false), Cancelled(false) { };
    #if ORYOL_HAS_ATOMIC
    std::atomic<bool> Handled;
    std::atomic<bool> Cancelled;
    #else
    bool Handled;
    bool Cancelled;
    #endif
};
} // namespace _priv;

//------------------------------------------------------------------------------
class POIORequest : public _priv::POioMsg {
    OryolClassDecl(POIORequest);
    OryolTypeDecl(POIORequest, _priv::POioMsg);
public:
    String type;//TODO utiliser un enum GET, POST, PUT
    String url;
    unsigned int port = 0;
    Buffer attachment;
    Oryol::Array<String> headers;
    Oryol::Array<String> urlParams;

    
//FIXME need much more data in here
//at least the headers, the url for GET
//the url parameters
//for PUT/POST, the FORM parameters, the PUT payload in case of PUTTING a file
    int StartOffset = 0;
    int EndOffset = EndOfFile;
    Buffer Data;
    POIOStatus::Code Status = POIOStatus::InvalidIOStatus;
    String ErrorDesc;
};

} // namespace Oryol
