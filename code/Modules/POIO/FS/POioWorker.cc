//------------------------------------------------------------------------------
//  POioWorker.cc
//------------------------------------------------------------------------------
#include "Pre.h"
#include "POioWorker.h"

namespace Oryol {
namespace _priv {

//------------------------------------------------------------------------------
POioWorker::POioWorker() :
threadStopRequested(false) {
    // empty
Log::Warn("construct POIWorker \n");
  
}

//------------------------------------------------------------------------------
void
POioWorker::start() {
    o_assert(!this->threadStartRequested);
    #if ORYOL_HAS_THREADS
        this->sendThreadId = std::this_thread::get_id();
        this->thread = std::thread(threadFunc, this);
    #endif
    this->threadStartRequested = true;
}

//------------------------------------------------------------------------------
void
POioWorker::stop() {
    o_assert(this->threadStartRequested);
    this->threadStopRequested = true;
    #if ORYOL_HAS_THREADS
        this->transferCondVar.notify_one();
        this->thread.join();
    #endif
    this->threadStopped = true;
}

//------------------------------------------------------------------------------
void
POioWorker::put(const Ptr<POioMsg>& msg) {

 Log::Info("enter POioWorker::put \n");
  
    o_assert(this->isSendThread());
    o_assert(this->threadStartRequested);
    o_assert(!this->threadStopped);
    this->writeQueue.Enqueue(msg);
}

//------------------------------------------------------------------------------
void
POioWorker::doWork() {
    // move messages to transfer queue and wake up thread if work needs to be done
    o_assert(this->isSendThread());
    o_assert(this->threadStartRequested);
    o_assert(!this->threadStopped);
    if (!this->writeQueue.Empty()) {
        this->moveWriteToTransferQueue();
    }
    o_assert_dbg(this->writeQueue.Empty());

    #if ORYOL_HAS_THREADS
    {
        std::lock_guard<std::mutex> lock(this->transferMutex);
        if (!this->transferQueue.Empty()) {
            this->transferCondVar.notify_one();
        }
    }
    #endif

    #if !ORYOL_HAS_THREADS
        // if platform has no threads, pump the message queue right
        // FIXME: we could do without all those queue transfers here!
        this->moveTransferToReadQueue();
        while (!this->readQueue.Empty()) {
            this->onMsg(std::move(this->readQueue.Dequeue()));
        }
    #endif
}

//------------------------------------------------------------------------------
#if ORYOL_HAS_THREADS
void
POioWorker::threadFunc(POioWorker* self) {
    self->workThreadId = std::this_thread::get_id();

    // the message processing loop waits for messages to arrive,
    // moves them from the transfer queue, processes them then goes back to sleep
    while (!self->threadStopRequested) {

        // wait for messages to arrive, and if so, transfer to read queue
        {
            std::unique_lock<std::mutex> lock(self->transferMutex);
            self->transferCondVar.wait(lock);
            self->moveTransferToReadQueue();
            lock.unlock();
        }

        // now process the messages, this happens without locking
        while (!self->readQueue.Empty()) {
            self->onMsg(std::move(self->readQueue.Dequeue()));
        }
    }
}
#endif

//------------------------------------------------------------------------------
bool
POioWorker::isSendThread() {
    #if ORYOL_HAS_THREADS
        return std::this_thread::get_id() == this->sendThreadId;
    #else
        return true;
    #endif
}

//------------------------------------------------------------------------------
bool
POioWorker::isWorkerThread() {
    #if ORYOL_HAS_THREADS
        return std::this_thread::get_id() == this->workThreadId;
    #else
        return true;
    #endif
}

//------------------------------------------------------------------------------
void
POioWorker::moveWriteToTransferQueue() {

Log::Info("enter POioWorker::moveWriteToTransferQueue \n");
  
    o_assert(this->isSendThread());
    // if the transfer queue is empty, we can do a very fast complete move
    {
        #if ORYOL_HAS_THREADS
        std::lock_guard<std::mutex> lock(this->transferMutex);
        #endif
        if (this->transferQueue.Empty()) {
            this->transferQueue = std::move(this->writeQueue);
        }
        else {
            // otherwise move messages one by one
            while (!this->writeQueue.Empty()) {
                this->transferQueue.Enqueue(this->writeQueue.Dequeue());
            }
        }
    }
}

//------------------------------------------------------------------------------
void
POioWorker::moveTransferToReadQueue() {
    o_assert(this->isWorkerThread());
    o_assert(this->readQueue.Empty());
    this->readQueue = std::move(this->transferQueue);
}

//------------------------------------------------------------------------------
bool
POioWorker::checkCancelled(const Ptr<POIORequest>& msg) {
    if (msg->Cancelled) {
        msg->Status = POIOStatus::Cancelled;
        msg->Handled = true;
        return true;
    }
    else {
        return false;
    }
}

//------------------------------------------------------------------------------
void
POioWorker::onMsg(const Ptr<POioMsg>& msg) {

Log::Info("enter POioWorker::onMsg \n");

  
    if (msg->IsA<POIORequest>()) {
        // find filesystem and forward request, NOTE:
        // the filesystem is responsible to set the
        // request to 'handled'!
        Ptr<POIORequest> ioReq = msg->DynamicCast<POIORequest>();
        if (!this->checkCancelled(ioReq)) {

	  // soit j'ai une seule instance de 'file system' reentrante
	  //soit j'en ai une par worker

	  //ne pas oublier a la fin msg->Handled = true;
	  http.onMsg(ioReq);


	  
            //Ptr<FileSystem> fs = this->fileSystemForURL(ioReq->Url);
            //if (fs) {
              // FIXME ici j'ai un seul FS qui implemente  fs->onMsg(ioReq);
//donc en garder une instance et appeler directement onMsg
            //le POioMsg ne contient pas forcement toutes les informations
            //utiles pour moi, pour construire effectivement la requete
            //}
        }
    }
}

} // namespace _priv
} // namespace Oryol
