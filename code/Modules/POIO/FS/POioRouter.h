#pragma once
//------------------------------------------------------------------------------
/**
    @class Oryol::_priv::POioRouter
    @ingroup IO
    @brief route IO requests to POioWorkers
*/
#include "Core/Containers/StaticArray.h"

#include "POIO/Core/POIOConfig.h"
#include "POIO/FS/POioWorker.h"

namespace Oryol {
namespace _priv {

class POioRouter {
public:
    /// setup the router
    void setup();
    /// discard the router
    void discard();
    /// route a ioMsg to one or more workers
    void put(const Ptr<POioMsg>& msg);
    /// perform per-frame work
    void doWork();

private:
    int curWorker = 0;
    StaticArray<POioWorker, POIOConfig::NumWorkers> workers;
};

} // namespace _priv
} // namespace Oryol

